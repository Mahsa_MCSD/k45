
# Khodro45 Task
cars's list implemmentation.



## Installation

Install project with npm or yarn

```bash
yarn yarn dev
npm start npm run dev
```
used json-server for api so please in other powerShell or command prompt type
specific port need for conflict of port 3000 nextjs and json-server 3000 port default
```bash
 json-server --watch db.json --port 8000 
```
    
## Environment Variable
axios config of Environmentvariable used in this project and be see in 

api>axios.js

`baseURL: 'http://localhost:8000/'`


## Tech Stack

**Client:** React, Redux, Scss, Nextjs, Axios, Thunk, pwa




## Usage/Examples
fetch products by pagination using axios 
used custom hooks useProducts and infinite scroll using zero npm package
```javascript
const useProducts = (pageNum = 1) => {
}
```
In Redux used thunk as a middleware and handle async requests
redux/product/product.action product.reducer product.types
```javascript
export const store = createStore(
  rootReducer,
  applyMiddleware(...middlewares, thunk)
);
```
inserted combine reducer only for future development
```javascript
const appReducer  = combineReducers({
    product:productReducer
  });
```
pwa with next-pwa and manifest

Scss use mixin for shared Usage and some variables for easy access
```javascript
%center-display {
    flex-direction: row;
    align-items: center
}
:root {
  --color-primary-light: #9d9d9d;
  --color-primary-dark: #31343e;
  ...
  }
```

## Feedback

If you have any feedback, please reach out to me at mahsaashrafi71@gmail.com

