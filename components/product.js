import Image from 'next/image'
import React from 'react'
import styles from './product.module.scss'

const Product = React.forwardRef(({ product }, ref) => {
    const productHeader = (
        <>
            <header className={styles.card_header}>
                <div className={styles.card_cover}>
                    <div className={`${styles.card_feature} text-12`}>
                            <Image width="18" height="18" src="/images/warranty.png" alt="loading" />
                        <span className={`mr-1 ml-1`}>
                            {product.feature.garantee}
                        </span>
                    </div>
                    <img className={styles.card_img} src={product.image} alt="image" />
                    <div className={`${styles.card_view_placeholder} text-white text-12`}>
                        <span className={`${styles.card_view_icon} ml-1`}>{product.viewCount}</span>
                        <Image width="16" height="16" src="/images/eye.png" alt="loading" />
                    </div>
                </div>
            </header>
        </>
    )
    const productPriceType = () => {
        switch (product.priceType) {
            case 'عالی':
                return <div className={`${styles.card_price_type_inner} my-1 d-flex text-12 font-weight-bold green`}>
                    <Image width="24" height="25" src="/images/superHappyFace.png" alt="loading" />
                    <span className='mr-1'>قیمت {product.priceType}</span>
                </div>
            case 'خوب':
                return <div className={`${styles.card_price_type_inner} my-1 d-flex text-12 font-weight-bold blue`}>
                    <Image width="24" height="25" src="/images/happyMediumFace.png" alt="loading" />
                    <span className='mr-1'>قیمت {product.priceType}</span>
                </div>
            case 'عادی':
                return <div className={`${styles.card_price_type_inner} my-1 d-flex text-12 font-weight-bold yellow`}>
                    <Image width="24" height="25" src="/images/happyFace.png" alt="loading" />
                    <span className='mr-1'>قیمت {product.priceType}</span>
                </div>
            default:
                return 
        }
    }



    const productBody = (
        <div className={styles.card_detail}>
            <div className={`${styles.card_title} text-16`}>
                <span className={`${styles.card_title_text} font-weight-bold mr-0`}>{product.brand}</span>
                <span className={styles.card_title_seperarator}></span>
                <span className={`${styles.card_title_text} font-weight-bold`}>{product.name}</span>
                <span className={styles.card_title_seperarator}></span>
                <span className={`${styles.card_title_text} font-weight-bold`}>{product.year}</span>
            </div>
            <div className={`${styles.card_price} d-flex font-weight-800 text-14`}>
                <div className={`${styles.card_price_number} py-1`}>

                    {product.price.toLocaleString()}
                    <span className="mr-1 text-10">تومان</span>
                </div>
                <div className={`${styles.card_price_discount} text-12`}>
                    {(product.price * (100 - product.feature.discount) / 100).toLocaleString()}
                </div>
                <div className={`${styles.card_Background_icon} bg-red`}>
                    <Image width="21" height="21" src="/images/discount.png" alt="discount" />
                </div>

            </div>
            <div className={`${styles.card_price_type} d-flex`}>
                {productPriceType()}

            </div>
            <div className={`${styles.card_digest} text-10 text-md-12 d-flex`}>
                <span className={styles.card_digest_single}>
                    {product.mileage.toLocaleString()} ک

                </span>
                <div className={`${styles.card_digist_seperator_dot}`}>

                </div>
                <div className={styles.card_digest_single}>
                    {product.gear}
                </div>
                <div className={`${styles.card_digist_seperator_dot}`}>

                </div>
                <div className={styles.card_digest_single}>
                    {product.documentType}
                </div>
            </div>
            <div className={`${styles.card_location} text-10 text-md-12 d-flex`}>
                <div className={`${styles.card_Background_icon} bg-gray`}>
                    <Image width="15" height="15" src="/images/location.png" alt="location" />
                </div>
                <span className={`${styles.card_location_text} font-weight-bold`}>{product.location}</span>

            </div>
        </div>
    )

    const content = ref
        ? <div className={styles.card} ref={ref}>
            <a className={styles.card_placeholder}>

                {productHeader}
                {productBody}
            </a>
        </div>
        : <div className={styles.card}>
            <a className={styles.card_placeholder}>

                {productHeader}
                {productBody}
            </a>
        </div>

    return content
})
Product.displayName = "Product";

export default Product