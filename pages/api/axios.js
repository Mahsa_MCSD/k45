import axios from 'axios'

export const api = axios.create({
    baseURL: 'http://localhost:8000/'
})

export const getProductPage = async (pageParam = 1, options = {}) => {
    const response = await api.get(`/products?_page=${pageParam}&_limit=5`, options)
    return response.data
}