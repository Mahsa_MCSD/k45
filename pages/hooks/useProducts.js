import { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { saveProducts } from '../../redux/product/product.action'
import { getProductPage } from '../api/axios'


const useProducts = (pageNum = 1) => {
  const dispatch= useDispatch();

    const products = useSelector(state => state.product.products)
    const [isLoading, setIsLoading] = useState(false)
    const [isError, setIsError] = useState(false)
    const [error, setError] = useState({})
    const [hasNextPage, setHasNextPage] = useState(false)
    useEffect(() => {
        setIsLoading(true)
        setIsError(false)
        setError({})

        const controller = new AbortController()
        const { signal } = controller

        getProductPage(pageNum, { signal })
            .then(data => {
                dispatch(saveProducts([...products, ...data]))
                setHasNextPage(Boolean(data.length))
                setIsLoading(false)
            })
            .catch(e => {
                setIsLoading(false)
                if (signal.aborted) return
                setIsError(true)
                setError({ message: e.message })
            })

        return () => controller.abort()

    }, [pageNum])

    return { isLoading, isError, error, products, hasNextPage }
}

export default useProducts