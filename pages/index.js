import Head from 'next/head'
import styles from '../styles/Home.module.scss'
import { useState, useRef, useCallback } from 'react'
import useProducts from './hooks/useProducts';
import Product from '../components/product';
import Image from 'next/image';
export default function Home() {
  const [pageNum, setPageNum] = useState(1)
  const {
    isLoading,
    isError,
    error,
    products,
    hasNextPage
  } = useProducts(pageNum)


  const intObserver = useRef()
  const lastPostRef = useCallback(product => {
    if (isLoading) return

    if (intObserver.current) intObserver.current.disconnect()

    intObserver.current = new IntersectionObserver(products => {
      if (products[0].isIntersecting && hasNextPage) {
        setPageNum(prev => prev + 1)
      }
    })

    if (product) intObserver.current.observe(product)
  }, [isLoading, hasNextPage])

  if (isError) return <p className='center'>Error: {error.message}</p>

  const content = products.map((product, i) => {
    if (products.length === i + 1) {
      return <Product ref={lastPostRef} key={product.id} product={product} />
    }
    return <Product key={product.id} product={product} />
  })
  return (
    <div className={`${styles.container}`}>
      <Head>
        <title>khodro45</title>
        <meta name="description" content="khodro45" />
      </Head>
      <div className={styles.grid} >
        <h1 id="top"></h1>
        {content}

      </div>
      {isLoading && <Image width="100" height="100" src="/images/k45loading.gif" alt="loading" />
      }
      <p className="center"><a href="#top"><Image width="50" height="50" src="/images/top.png" alt="top" /></a></p>
    </div>
  )
}
