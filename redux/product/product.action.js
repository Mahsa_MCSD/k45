import { ProductActionTypes } from './product.types';


export const saveProducts=(item)=>(dispatch)=>{
    dispatch({
        type:ProductActionTypes.SAVE_PRODUCTS,
        payload:item
    })
}